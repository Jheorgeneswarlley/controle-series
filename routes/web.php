<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


use Illuminate\Support\Facades\Auth;

Route::get('/series', 'SeriesController@index')->name('listar_series');
Route::get('/series/criar', 'SeriesController@create')->name('form_criar_serie')->middleware('autenticador'); //Usando autenticação do usuário
Route::post('/series/criar', 'SeriesController@store')->middleware('autenticador'); //Usando autenticação do usuário
Route::delete('/series/{id}', 'SeriesController@destroy')->middleware('autenticador'); //Usando autenticação do usuário
Route::post('/series/{id}/editaNome', 'SeriesController@editaNome');

Route::get('/series/{serieId}/temporadas', 'TemporadasController@index');

Route::get('/temporadas/{temporada}/episodios', 'EpisodiosController@index');
Route::post('/temporadas/{temporada}/episodios/assistir', 'EpisodiosController@assistir')->middleware('autenticador'); //Usando autenticação do usuário

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/entrar', 'EntrarController@index');
Route::post('/entrar', 'EntrarController@entrar');
Route::get('/registrar', 'RegistroController@create');
Route::post('/registrar', 'RegistroController@store');

Route::get('/sair', function(){
    Auth::logout();
    return redirect('/entrar');
});

Route::get('/visualizando-email', function(){
   return new \App\Mail\NovaSerie( //Chamada da classe cuja a estrutura seja de uma classe de Email
       'Arrow', //Nome da Série
       '5', //Quantidade de Temporadas
       '10' //Quantidade de Episódios
   );
});

Route::get('/enviando-email', function(){
    $email = new \App\Mail\NovaSerie(
        'Arrow',
        '5',
        '10'
    );

    $email->subject = 'Nova Série Adicionada'; //Adicionando um título ao email

    /* Transformando os dados de email no array em objeto */
    $user = (object) [
        'email' => 'jheorgenes@teste.com',
        'name' => 'jheorgenes'
    ];

    \Illuminate\Support\Facades\Mail::to($user)->send($email); //Utilizando o fascade de email para enviar para o usuário correto.
    return 'Email enviado!';
});
